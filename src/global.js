import Vue from 'vue'

export const userKey = '_sanfitness_user'
export const cartKey = '_sanfitness_cart'
export const baseApiUrl = 'https://sanfitness-api.fly.dev'

export function showError(e) {
    if( e && e.response && e.response.data) {
        Vue.toasted.global.defaultError({ msg: e.response.data })
    } else if(typeof e === 'string') {
        Vue.toasted.global.defaultError({msg: e})
    } else {
        Vue.toasted.global.defaultError()
    }
}

export function showSuccess(msg){
    Vue.toasted.global.defaultSuccess({msg: msg})
}