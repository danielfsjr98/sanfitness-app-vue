import api from 'axios'
import { baseApiUrl, showError, showSuccess, cartKey } from '@/global'
import store from '@/store'

export default {
    async setSale({ commit }, _payload) {
        const userId = store.getters['Auth/getUser'].id
        const payload = {userId, sales: _payload}
        return await api.post(`${baseApiUrl}/sales`,  payload)
            .then(res => {
                commit("SALE", res.data);
                showSuccess("Pedido enviado com sucesso!")
                return true
            })
            .catch(err => {
                showError(err)
                commit("SALE", err.response.data)
                return false
            })
    },
    setProductToCart({ commit }, _payload) {
        const userId = store.getters['Auth/getUser'].id
        let cart = localStorage.getItem(cartKey) ? JSON.parse(localStorage.getItem(cartKey)) : []
        if(cart.length > 0) {
            cart = cart[0].userId !== userId ? cart = [] : cart
        }
        if(_payload){
            if(!_payload.amount){
                showError("Informe a quantidade.")
                return false
            }
            if(parseInt(_payload.amount) <= 0){
                showError("A quantidade deve ser positiva.")
                return false
            }
            if(parseInt(_payload.amount) > parseInt(_payload.quantityInStock)){
                showError("Quantidade solicitada maior que a disponível.")
                return false
            }
            _payload.userId = userId
            const equalProducts = cart.filter(product => _payload.id === product.id)
            if(equalProducts.length > 0){
                const total = equalProducts.map(sale => sale.amount)
                                .reduce((accumulator, currentValue) => parseFloat(accumulator) + parseFloat(currentValue))                
                const checkMax = parseInt(total) + parseInt(_payload.amount)
                if(checkMax > _payload.quantityInStock){
                    showError("Você está adicionando mais do que o disponível.")
                    return false
                }
            }
            cart.push(_payload)
            showSuccess("Produto adicionado ao carrinho!")
        }
        commit("CART", cart);
        localStorage.setItem(cartKey, JSON.stringify(cart))
    },
    clearCart({ commit }){
        localStorage.setItem(cartKey, JSON.stringify([]))
        commit("CART", []);
    },
    removeItemFromCart({ commit }, payload){
        const cart = JSON.parse(localStorage.getItem(cartKey))
        cart.splice(payload, 1)
        localStorage.setItem(cartKey, JSON.stringify(cart))
        commit("CART", cart);
    },
    async setMySales({ commit }, _payload) {
        await api.get(`${baseApiUrl}/user/${_payload.userId}/sales?page=${_payload.page}`)
            .then(res => commit("MY_SALES", res.data))
            .catch(showError)
    },
    async setAllSales({ commit }) {
        await api.get(`${baseApiUrl}/sales`)
            .then(res => commit("ALL_SALES", res.data))
            .catch(showError)
    },
    async setSalesItemsBySaleId({ commit }, payload) {
        await api.get(`${baseApiUrl}/sale/${payload}/items`)
            .then(res => commit("SALES_ITEMS_BY_SALE_ID", res.data))
            .catch(showError)
    },
    async removeSale({ commit }, payload){
        await api.delete(`${baseApiUrl}/sales/${payload}`)
            .then(res => {
                showSuccess("Pedido excluído com sucesso!")
                commit("SALE", res.data)
            })
            .catch(msg => {
                showError(msg)
                commit("SALE", null)
            })
    },
    async editSale({ commit }, payload){
        await api.put(`${baseApiUrl}/sales/${payload.id}`, payload)
            .then(() => {
                showSuccess("Pedido atualizado com sucesso!")
                commit("SALE", payload)
            })
            .catch(msg => {
                showError(msg)
                commit("SALE", null)
            })
    },
    async setSaleById({ commit }, payload) {
        await api.get(`${baseApiUrl}/sales/${payload}`)
            .then(res => commit("SALE", res.data))
            .catch(showError)
    },
}
  