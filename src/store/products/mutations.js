export default {
    ALL_PRODUCTS(state, payload) {
        state.products = payload
    },
    PRODUCT(state, payload) {
        state.product = payload
    },
    PRODUCT_BY_ID(state, payload) {
        state.productById = payload
    },
    ALL_PRODUCTS_BY_REF(state, payload) {
        state.allProductsByRef = payload
    },
    PRODUCT_BY_REF(state, payload) {
        state.productByRef = payload
    },
    AVAILABLE_PRODUCTS_BY_REF(state, payload) {
        state.availableProductsByRef = payload
    },
}