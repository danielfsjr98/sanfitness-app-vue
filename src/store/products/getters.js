export default {
    getProducts(state) {
        return state.products
    },
    getProduct(state) {
        return state.product
    },
    getProductById(state) {
        return state.productById
    },
    getAllProductsByRef(state) {
        return state.allProductsByRef
    },
    getProductByRef(state) {
        return state.productByRef
    },
    getAvailableProductsByRef(state) {
        return state.availableProductsByRef
    },
    
}