export default {
  setLoading({ commit }) {
    commit('SET_LOADING', true);
  },
  clearLoading({ commit }) {
    commit('CLEAR_LOADING', false);
  },
};
