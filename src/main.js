import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import store from './store/'
import './config/msgs'
import './config/axios'
import router from './config/router'
import moment from 'moment-timezone'
import VueMoment from 'vue-moment'

Vue.config.productionTip = false

Vue.use(VueMoment, { moment });
moment.tz.setDefault('Brasil/São_Paulo');
moment.locale('pt-br');

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
