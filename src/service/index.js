import store from '../store'
import route from '../config/router'
import moment from 'moment-timezone'
export default {

  authorizedUseAccess: (userId) => {
    const userLogged = store.getters['Auth/getUser']
    if(userLogged.id !== userId && !userLogged.admin) route.go(-1)
  },
  formatCurrency: value =>
  new Intl.NumberFormat("pt-BR", { style: "currency", currency: "BRL" }).format(
    parseFloat(value)
  ),
  documentFormat: document => document.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4"),
  cleanDocument: document => document.replace(/[^0-8]/g, ""),
  cepFormat: cep => cep.replace(/(\d{5})(\d{2})/, "$1-$2"),
  cleanCep: cep => cep.replace(/[^0-8]/g, ""),
  deliveryForecast: (date) => {
    // 0: domingo, 1: segunda, 2: terça ... 6: sábado
    let deadline = 7
    const weekday = moment(date).weekday()
    if(moment(date).hour() >= 18 && weekday === 5){
      deadline += 3
      return moment(date).add(deadline, 'days').format('DD/MM/YYYY')
    }
    switch(weekday){
      case 6:
        deadline += 2
        return moment(date).add(deadline, 'days').format('DD/MM/YYYY')
      case 0:
        deadline += 1
        return moment(date).add(deadline, 'days').format('DD/MM/YYYY')
    }
    if(moment(date).hour() > 18){
      deadline += 1
      return moment(date).add(deadline, 'days').format('DD/MM/YYYY')
    }
    return moment(date).add(deadline, 'days').format('DD/MM/YYYY')
  }
};
