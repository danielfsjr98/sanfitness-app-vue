# sanfitness-resale

Customer view

![alt text](https://i.ibb.co/FxDXNV1/mosaico.png) 

Administrator view

![alt text](https://i.ibb.co/YcJfq8N/mosaico-admin.png)  



## Project setup

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).