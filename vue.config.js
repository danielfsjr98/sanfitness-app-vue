module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  devServer: {
    allowedHosts: [
      '.ngrok.io'
    ],
  }
}
